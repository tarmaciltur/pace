_pace_completions()
{
  local cur prev

  COMPREPLY=()
  cur=${COMP_WORDS[COMP_CWORD]}
  prev=${COMP_WORDS[COMP_CWORD-1]}

  if [ $COMP_CWORD -eq 1 ]
  then
    COMPREPLY=($(compgen -W "$(pace --help | grep '  ' | grep -v ':' | sed 's/, /\n/' | cut -d ' ' -f 1)" -- $cur))
  elif [ $COMP_CWORD -eq 2 ]
  then
    case "$prev" in
      "enable")
        COMPREPLY=( $(compgen -W "$(pace list | grep '(' | cut -d ' ' -f 1)" -- $cur) )
        ;;
      "disable")
        COMPREPLY=( $(compgen -W "$(pace list | grep -v '(' | cut -d ' ' -f 1)" -- $cur) )
        ;;
      "move")
        COMPREPLY=( $(compgen -W "$(pace list | cut -d ' ' -f 1)" -- $cur) )
        ;;
      "remove")
        COMPREPLY=( $(compgen -W "$(pace list | cut -d ' ' -f 1)" -- $cur) )
        ;;
      "set")
        COMPREPLY=( $(compgen -W "$(pace list-options | cut -d ' ' -f 1)" -- $cur) )
        ;;
      "unset")
        COMPREPLY=( $(compgen -W "$(pace list-options | grep -v '(' | cut -d ' ' -f 1)" -- $cur) )
        ;;
      *)
        ;;
    esac
    elif [ $COMP_CWORD -eq 3 ]
    then
      first=${COMP_WORDS[1]}
      case "$first" in
        "move")
          REPOSITORIES=($(pace list | cut -d ' ' -f 1))
          OPTIONS="--above --below"
          if [ ${REPOSITORIES[0]} != $prev ]
          then
            OPTIONS=$OPTIONS" --up --first"
          fi
          if [ ${REPOSITORIES[-1]} != $prev ]
          then
            OPTIONS=$OPTIONS" --down --last"
          fi
          COMPREPLY=( $(compgen -W "$OPTIONS" -- $cur) )
          ;;
        "add")
          OPTIONS="--server --include --siglevel --usage --above --below --disabled"
          COMPREPLY=( $(compgen -W "$OPTIONS" -- $cur) )
          ;;
      esac
    elif [ $COMP_CWORD -eq 4 ]
    then
      first=${COMP_WORDS[1]}
      case "$first" in
        "move")
          if [ "$prev" == "--above" ] || [ "$prev" == "--below" ]
          then
            REPO=${COMP_WORDS[2]}
            REPOSITORIES=$(pace list | cut -d ' ' -f 1 | grep -v $REPO)
            COMPREPLY=( $(compgen -W "$REPOSITORIES" -- $cur) )
          fi
          ;;
        "add")
          OPTIONS=""
          REPOSITORIES=""
          if [ "$prev" == "--disabled" ]
          then
            OPTIONS="--server --include --siglevel --usage --above --below"
          elif [ "$prev" == "--above" ] || [ "$prev" == "--below" ]
          then
            REPOSITORIES=$(pace list | cut -d ' ' -f 1)
          fi

          COMPREPLY=( $(compgen -W "$OPTIONS $REPOSITORIES" -- $cur) )
          ;;
      esac
    elif [ $COMP_CWORD -gt 4 ]
    then
      first=${COMP_WORDS[1]}
      case "$first" in
        "add")
          OPTIONS=""
          REPOSITORIES=""
          if [ "$prev" == "--above" ] || [ "$prev" == "--below" ]
          then
            REPOSITORIES=$(pace list | cut -d ' ' -f 1)
          elif [[ ! "--server --include --siglevel --usage --above --below" =~ "${prev}" ]]
          then
            OPTIONS="--server --include --siglevel --usage"
            if [[ ! "${COMP_WORDS[@]}" =~ "--below" ]] && [[ ! "${COMP_WORDS[@]}" =~ "--above" ]]
            then
              OPTIONS=$OPTIONS" --below"
            fi
            if [[ ! "${COMP_WORDS[@]}" =~ "--above" ]] && [[ ! "${COMP_WORDS[@]}" =~ "--below" ]]
            then
              OPTIONS=$OPTIONS" --above"
            fi
            if [[ ! "${COMP_WORDS[@]}" =~ "--disabled" ]]
            then
              OPTIONS=$OPTIONS" --disabled"
            fi
          fi
          COMPREPLY=( $(compgen -W "$OPTIONS $REPOSITORIES" -- $cur) )
          ;;
      esac
    fi
}

complete -F _pace_completions pace
