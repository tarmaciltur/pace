/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_repository.vala
 *
 * Copyright 2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using IniParser;

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_row_repository_grid.ui" )]
  public class RowRepositoryGrid : Gtk.Grid {
    [GtkChild]
    private Gtk.Label label_name;
    [GtkChild]
    private Gtk.Label label_no_keys;
    [GtkChild]
    private ListBoxFixed listbox_keys;
    [GtkChild]
    private Gtk.Revealer revealer_options;

    construct {}

    public ListBox get_keys () {
      return this.listbox_keys;
    }

    public string get_name () {
      return this.label_name.get_label ();
    }

    public void set_name ( string name ) {
      this.label_name.set_label ( name );
    }

    public void set_key ( Key key ) {
      this.listbox_keys.insert_row_label_label ( key.get_key (), -1, key.get_value () );
    }

    public void set_keys ( ListBox keys ) {
      this.listbox_keys.clear ();

      RowLabelEntry row = keys.get_row_at_index ( 0 ).get_child () as RowLabelEntry;
      this.set_name ( row.get_value () );

      for ( int i = 1; i < keys.length; i++ ) {
        var row_combobox = keys.get_row_at_index ( i ).get_child () as RowComboBoxEntry;

        var key = new Key ( row_combobox.get_key (), row_combobox.get_value (), KeyWithValue.YES );

        this.set_key ( key );
      }
    }

    public new void on_row_activated () {
      bool revealed_child = this.revealer_options.get_reveal_child ();

      if ( revealed_child ) {
        this.revealer_options.set_reveal_child ( false );
      } else {
        this.revealer_options.set_reveal_child ( true );
      }
    }

    [GtkCallback]
    private void listbox_length_changed () {
      if ( this.listbox_keys.length != 0 ) {
        this.listbox_keys.set_visible ( true );
        this.label_no_keys.set_visible ( false );
      } else {
        this.listbox_keys.set_visible ( false );
        this.label_no_keys.set_visible ( true );
      }
    }
  }
}
