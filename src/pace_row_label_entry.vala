/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_label_entry.vala
 *
 * Copyright 2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_row_label_entry.ui" )]
  public class RowLabelEntry : RowLabel, RowKeyValue {
    [GtkChild]
    private Gtk.Entry entry_value;
    public bool entry_visible {
      get { return this.entry_value.get_visible (); }
      set { this.entry_value.set_visible ( value );}
    }

    public signal void changed ();

    public RowLabelEntry ( string nombre, string value = "" ) {
      base ( nombre );

      this.entry_value.set_text ( value );
    }

    public void set_key ( string key ) {
      this.set_label_key ( key );
    }

    public string get_key () {
      return this.get_label_key ();
    }

    public string get_value () {
      return this.entry_value.get_text ();
    }

    public void set_value ( string value ) {
      this.entry_value.set_text ( value );
    }

    [GtkCallback]
    private void entry_changed () {
      this.changed ();
    }
  }
}
