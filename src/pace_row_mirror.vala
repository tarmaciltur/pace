/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_mirror.vala
 *
 * Copyright 2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using IniParser;

namespace Pace {
  public class RowMirror : RowSortable {
    private string initial_data;

    construct {
      var row = new RowLabelLabel ( "Server" );
      row.label_value_halign = Gtk.Align.START;
      row.bold = true;
      row.margin_bottom = 0;

      this.row_widget = row;
      this.edit_form.toolbar_add_button_visible = false;

      this.set_delete_message ( _("mirror" ) );

      this.button_edit_clicked.connect ( this.complete_edit_form );
      this.edit_save.connect ( this.save_edit );
      this.edit_cancel.connect ( this.cancel_edit );
    }

    public void set_mirror_data ( Key datos ) {
      var row = this.row_widget as RowLabelLabel;
      if ( row != null ) {
        row.set_key ( datos.get_key () );
        row.set_value ( datos.get_value () );
      }
      this.set_enabled ( datos.get_commented () == Commented.NO );
      this.initial_data = to_config_key ().to_string ();
    }

    public bool get_changed () {
      return this.to_config_key ().to_string () != this.initial_data;
    }

    public Key to_config_key () {
      var row = this.row_widget as RowLabelLabel;
      var key = new Key ( "", "", KeyWithValue.YES, Commented.YES );
      if ( row != null ) {
        Commented commented = this.get_enabled () ? Commented.NO : Commented.YES;

        key = new Key ( row.get_key (), row.get_value (), KeyWithValue.YES, commented );
      }
      return key;
    }

    private void complete_edit_form () {
      var row = this.row_widget as RowLabelLabel;
      if ( row != null ) {
        this.edit_form.insert_row_label_entry ( row.get_key (), row.get_value () );
      }
    }

    private void save_edit () {
      var keys = this.edit_form.get_listbox ();

      debug ( keys.length.to_string () );
      var key = keys.get_row_at_index ( 0 ).get_child () as RowLabelEntry;

      var row = this.row_widget as RowLabelLabel;
      if ( row != null ) {
        row.set_key ( key.get_key () );
        row.set_value ( key.get_value () );
      }

      keys.clear ();

      this.changed ();
    }

    private void cancel_edit () {
      var row = this.row_widget as RowLabelLabel;
      if ( row != null ) {
        if ( row.get_value () == "" ) {
          this.on_button_delete_accept_clicked ();
        }
      }
    }

    public void new_empty () {
      this.on_button_edit_clicked ();
    }
  }
}
