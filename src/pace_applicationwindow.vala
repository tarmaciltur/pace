/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_applicationwindow.vala
 *
 * Copyright (C) 2018-2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_applicationwindow.ui" )]
  public class ApplicationWindow : Gtk.ApplicationWindow {
    [GtkChild]
    private Gtk.Button button_back;
    [GtkChild]
    private Gtk.Button button_cancel;
    [GtkChild]
    private Gtk.Button button_preview;
    [GtkChild]
    private Gtk.Button button_save;
    [GtkChild]
    private Gtk.Button button_save_direct;
    [GtkChild]
    private Configuration configuration;
    [GtkChild]
    private Gtk.MenuButton menubutton_menu;
    [GtkChild]
    private Gtk.HeaderBar headerbar;
    [GtkChild]
    private Gtk.SourceView sourceview_preview;
    [GtkChild]
    private Gtk.Stack stack_editors;
    [GtkChild]
    private Gtk.Stack stack_main;
    [GtkChild]
    private SelectorMirrorlistFiles selector_mirrorlist_files;
    [GtkChild]
    private ListBoxOptions listbox_options;
    [GtkChild]
    private ListBoxRepositories listbox_repositories;
    [GtkChild]
    private Gtk.StackSwitcher stack_switcher_editors;
    private ConfigFile config_file;
    private Gtk.Label label_custom_title;
    private GLib.Settings settings;

    public ApplicationWindow ( Gtk.Application app ) {
      Object ( application: app );

      this.label_custom_title = new Gtk.Label ( "" );

      this.create_app_menu ();

      this.load_file ();

      this.listbox_repositories.change.connect ( this.current_file_change_check );
      this.listbox_options.change.connect ( this.current_file_change_check );
      selector_mirrorlist_files.change.connect ( this.current_file_change_check );

      if ( this.configuration.get_preview_enable_state () ) {
        this.button_preview.set_visible ( true );
      } else {
        this.button_save_direct.set_visible ( true );
      }

      this.settings = new Settings ( "ar.com.softwareperonista.Pace" );
      this.settings.changed["full-preview"].connect ( this.set_sourceview_lang );
      this.set_sourceview_lang ();
    }

    private void load_file () {
      this.config_file = new ConfigFile ( "/etc/pacman.conf" );

      this.selector_mirrorlist_files.set_mirrorlist_files ( this.config_file.get_mirrorlists () );
      this.listbox_options.set_options ( this.config_file );
      this.listbox_repositories.set_repositories ( this.config_file );
    }

    private void set_sourceview_lang () {
      var language_manager = new Gtk.SourceLanguageManager ();

      var buffer = this.sourceview_preview.buffer as Gtk.SourceBuffer;

      if ( this.settings.get_boolean ( "full-preview" ) ) {
        buffer.set_language ( language_manager.get_language ( "ini" ) );
      } else {
        buffer.set_language ( language_manager.get_language ( "diff" ) );
      }
    }

    private void page_needs_attention () {
      Array<DataStatus> same_file_pages = new Array<DataStatus> ();
      DataStatus current_page = this.stack_editors.get_visible_child () as DataStatus;
      bool need_attention = false;
      if ( current_page != null ) {
        same_file_pages.append_val ( current_page );
        need_attention = current_page.get_changed ();
        var children = this.stack_editors.get_children ();

        for ( int i = 0; i < children.length (); i++ ) {
          var page = children.nth_data ( i ) as DataStatus;
          if ( page.get_file_path () == current_page.get_file_path () ) {
            need_attention = need_attention || page.get_changed ();
            same_file_pages.append_val ( page );
          }
        }
      }

      for ( int i = 0; i < same_file_pages.length; i++ ) {
        this.stack_editors.child_set_property ( same_file_pages.index ( i ) as Gtk.Widget, "needs-attention", need_attention );
      }
    }

    private void reset_needs_attention () {
      var children = this.stack_editors.get_children ();

      for ( int i = 0; i < children.length (); i++ ) {
        this.stack_editors.child_set_property ( children.nth_data ( i ), "needs-attention", false );
      }
    }

    [GtkCallback]
    private void current_file_change_check () {
      bool changed = false;

      DataStatus current_page = this.stack_editors.get_visible_child () as DataStatus;

      if ( current_page != null ) {
        changed = current_page.get_changed ();

        if ( !changed ) {
          var children = this.stack_editors.get_children ();

          for ( int i = 0; i < children.length (); i++ ) {
            var page = children.nth_data ( i ) as DataStatus;
            if ( page.get_file_path () == current_page.get_file_path () ) {
              changed = page.get_changed ();
              if ( changed ) {
                break;
              }
            }
          }
        }
      }

      if ( this.configuration.get_preview_enable_state () ) {
        this.button_preview.set_sensitive ( changed );
      } else {
        this.button_save_direct.set_sensitive ( changed );
      }
      this.page_needs_attention ();

      if ( this.stack_editors.get_visible_child_name () != "page_mirrorlist" ) {
        debug ( "setting repositories" );
        this.config_file.set_repositories ( this.listbox_repositories.get_repositories () );
        debug ( "setting options" );
        this.config_file.set_options ( this.listbox_options.get_options () );
      }
    }

    [GtkCallback]
    private void on_button_preview_clicked () {
      var current_file = this.get_current_file ();

      if ( this.settings.get_boolean ( "full-preview" ) ) {
        this.sourceview_preview.buffer.text = current_file.to_string ();
      } else {
        this.sourceview_preview.buffer.text = current_file.diff ();
      }
      this.label_custom_title.set_label ( current_file.get_path () );


      this.label_custom_title.set_visible ( true );
      this.stack_main.set_visible_child_name ( "page_preview_changes" );
      this.headerbar.set_custom_title ( this.label_custom_title );
      this.button_cancel.set_visible ( true );
      this.button_preview.set_visible ( false );
      this.button_save.set_visible ( true );
      this.menubutton_menu.set_visible ( false );
      this.headerbar.set_show_close_button ( false );
    }

    [GtkCallback]
    private void on_button_cancel_clicked () {
      this.stack_main.set_visible_child_name ( "page_editors_view" );
      this.button_cancel.set_visible ( false );
      if ( this.configuration.get_preview_enable_state () ) {
        this.button_preview.set_visible ( true );
      } else {
        this.button_save_direct.set_visible ( true );
      }
      this.button_save.set_visible ( false );
      this.menubutton_menu.set_visible ( true );
      this.headerbar.set_custom_title ( this.stack_switcher_editors );
      this.headerbar.set_show_close_button ( true );
    }

    [GtkCallback]
    private void on_button_back_clicked () {
      this.stack_main.set_visible_child_name ( "page_editors_view" );
      this.button_back.set_visible ( false );
      if ( this.configuration.get_preview_enable_state () ) {
        this.button_preview.set_visible ( true );
      } else {
        this.button_save_direct.set_visible ( true );
      }
      this.button_save.set_visible ( false );
      this.menubutton_menu.set_visible ( true );
      this.headerbar.set_custom_title ( this.stack_switcher_editors );
      this.headerbar.set_show_close_button ( true );
    }

    [GtkCallback]
    private void on_button_save_clicked () {
      if ( FileHelper.write ( this.get_current_file ().get_path (), this.get_current_file ().to_string () ) ) {
        this.on_button_cancel_clicked ();
        this.load_file ();
        this.reset_needs_attention ();
      }
    }

    private void create_app_menu () {
      var builder = new Gtk.Builder ();
      try {
        builder.add_from_resource ( "/ar/com/softwareperonista/pace/ui/pace_main_menu.ui" );
        this.menubutton_menu.set_menu_model ( builder.get_object ( "main-menu" ) as  Menu );
      } catch (  Error e ) {
        error ( "loading ui file: %s", e.message );
      }
      var action = new SimpleAction ( "quit", null );
      action.set_enabled ( true );
      action.activate.connect ( () => { this.quit (); } );
      this.application.add_action ( action );

      action = new SimpleAction ( "about", null );
      action.set_enabled ( true );
      action.activate.connect ( () => { this.about (); } );
      this.application.add_action ( action );

      action = new SimpleAction ( "show_configuration", null );
      action.set_enabled ( true );
      action.activate.connect ( () => { this.show_configuration (); } );
      this.application.add_action ( action );
    }

    private void about () {
      string[] authors = {
        "Andres Fernandez <andres@softwareperonista.com.ar>",
        "Fernando Fernandez <fernando@softwareperonista.com.ar>",
        "Marco Parolo (" + _( "tester" ) + ")",
        "Ricardo Jeroncich (" + _( "tester" ) + ")"
      };
      Gtk.show_about_dialog ( this,
                              "authors", authors,
                              "program-name", "Pace",
                              "title", _("About" ) + " Pace",
                              "comments", _("A simple graphical tool to edit pacman.conf" ),
                              "copyright", "Copyright 2018 Fernando Fernández " + _("and" ) + " Andres Fernández",
                              "license-type", Gtk.License.GPL_3_0,
                              "logo-icon-name", "ar.com.softwareperonista.Pace",
                              "version", Config.VERSION,
                              "website", "https://github.com/softwareperonista/pace",
                              "wrap-license", true );
    }

    private void quit () {
      this.close ();
    }

    private void show_configuration () {
      this.label_custom_title.set_label ( "Configuration" );
      this.stack_main.set_visible_child_name ( "page_configuration" );
      this.label_custom_title.set_visible ( true );
      this.headerbar.set_custom_title ( this.label_custom_title );
      this.button_back.set_visible ( true );
      if ( this.configuration.get_preview_enable_state () ) {
        this.button_preview.set_visible ( false );
      } else {
        this.button_save_direct.set_visible ( false );
      }
      this.menubutton_menu.set_visible ( false );
    }

    private ConfigFile get_current_file () {
      var current = this.config_file;

      if ( this.stack_editors.get_visible_child_name () == "page_mirrorlist" ) {
        current = this.selector_mirrorlist_files.get_current_mirrorlist_file ();
      }

      debug ( "current: " + current.get_path () );
      return current;
    }
  }
}
