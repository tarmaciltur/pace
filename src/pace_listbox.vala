/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_listbox.vala
 *
 * Copyright 2018-2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_listbox.ui" )]
  public class ListBox : Gtk.Grid {
    [GtkChild]
    protected Gtk.ListBox listbox;
    [GtkChild]
    private Gtk.Toolbar toolbar_add_button;
    [GtkChild]
    private Gtk.ScrolledWindow scrolled_window;
    public int length { get; private set; }
    public string add_button_tooltip { get; set; }
    public bool activatable { get; set; default = true; }
    public int min_content_width {
      get { return this.scrolled_window.min_content_width; }
      set { this.scrolled_window.min_content_width = value; }
    }
    public int max_content_width {
      get { return this.scrolled_window.max_content_width; }
      set { this.scrolled_window.max_content_width = value; }
    }
    public Gtk.PolicyType vscrollbar_policy {
      get { return this.scrolled_window.vscrollbar_policy; }
      set { this.scrolled_window.vscrollbar_policy = value; }
    }
    public bool toolbar_add_button_visible {
      get { return this.toolbar_add_button.get_visible (); }
      set { this.toolbar_add_button.set_visible ( value ); }
    }

    public signal void changed ();
    public signal void clicked_button_add_row ();
    public signal void row_activated ( Gtk.ListBoxRow row );

    construct {
      this.length = 0;
      this.changed ();
      this.listbox.selection_mode = Gtk.SelectionMode.NONE;

      this.notify["add-button-tooltip"].connect ( () => this.toolbar_add_button.set_tooltip_text ( this.add_button_tooltip ) );
    }

    [GtkCallback]
    private void on_row_activated ( Gtk.ListBoxRow row ) {
      this.row_activated ( row );
    }

    public void insert ( Gtk.Widget widget, int position ) {
      this.listbox.insert ( widget, position );
      this.length++;
      if ( !this.activatable ) {
        this.unset_activatable ( position );
      }
      this.changed ();
    }

    public Gtk.ListBoxRow get_row_at_index ( int index ) {
      return this.listbox.get_row_at_index ( index );
    }

    public void clear () {
      Gtk.ListBoxRow row = this.listbox.get_row_at_index ( 0 );

      while ( row != null ) {
        row.destroy ();
        row = this.listbox.get_row_at_index ( 0 );
      }

      this.length = 0;
    }

    [GtkCallback]
    public void on_clicked_button () {
      this.clicked_button_add_row ();

      while ( Gtk.events_pending () ) {
        Gtk.main_iteration ();
      }

      this.scrolled_window.vadjustment.value = this.scrolled_window.vadjustment.upper;
    }

    public void remove_listboxrow ( Gtk.ListBoxRow row ) {
      this.listbox.remove ( row );
      this.length--;
      this.changed ();
    }

    private void unset_activatable ( int position ) {
      if ( position == -1 ) {
        position = this.length - 1;
      }

      Gtk.ListBoxRow row = this.listbox.get_row_at_index ( position );
      row.set_activatable ( false );
    }

    public bool get_changed () {
      bool changed = false;
      int i = 0;
      var row = this.listbox.get_row_at_index ( i );

      while ( row != null ) {
        var row_mirror = row.get_child () as RowChangeable;

        if ( row_mirror.get_changed () ) {
          changed = true;
          break;
        }

        i++;
        row = this.listbox.get_row_at_index ( i );
      }

      return changed;
    }
  }
}
