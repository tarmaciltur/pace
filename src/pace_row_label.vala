/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_label.vala
 *
 * Copyright 2018 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_row_label.ui" )]
  public class RowLabel : Gtk.Grid, RowSizeGroup {
    [GtkChild]
    private Gtk.Label label_key;
    private bool _bold;
    public bool bold {
      get { return this._bold; }
      set { this._bold = value; this.on_bold_changed (); }
    }

    public RowLabel ( string key ) {
      this.label_key.set_label ( key );
    }

    public string get_label_key () {
      return this.label_key.get_label ();
    }

    public void set_label_key ( string key ) {
      this.label_key.set_label ( key );
    }

    public void set_size_group ( Gtk.SizeGroup size_group ) {
      size_group.add_widget ( this.label_key );
    }

    public void on_bold_changed () {
      var attributes = new Pango.AttrList ();

      if ( bold ) {
        attributes.insert ( Pango.attr_weight_new ( Pango.Weight.ULTRAHEAVY ) );
      } else {
        attributes.insert ( Pango.attr_weight_new ( Pango.Weight.NORMAL ) );
      }

      this.label_key.set_attributes ( attributes );
    }
  }
}
