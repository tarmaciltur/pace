/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_repository.vala
 *
 * Copyright 2018-2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using IniParser;

namespace Pace {
  public class RowRepository : RowSortable {
    private string initial_data;

    construct {
      this.row_widget = new RowRepositoryGrid ();

      this.set_delete_message ( _("repository" ) );

      this.button_edit_clicked.connect ( this.complete_edit_form );
      this.button_delete_clicked.connect ( this.complete_delete_form );
      this.edit_save.connect ( this.save_edit );
    }

    public new void on_row_activated () {
      var row = this.row_widget as RowRepositoryGrid;
      if ( row != null ) {
        row.on_row_activated ();
      }
    }

    public void set_repository_data ( Section repository ) {
      this.set_name ( repository.get_name () );
      bool enabled = repository.get_commented () == Commented.NO;
      this.set_enabled ( enabled );

      var keys = repository.get_keys ();
      for ( int j=0; j < keys.length; j++ ) {
        this.set_key ( keys.index ( j ) );
      }

      this.initial_data = this.to_config_repository ().to_string ();
    }

    public bool get_changed () {
      return this.to_config_repository ().to_string () != this.initial_data;
    }

    public void set_name ( string name ) {
      var row = this.row_widget as RowRepositoryGrid;
      if ( row != null ) {
        row.set_name ( name );
      }
    }

    public void set_key ( Key key ) {
      var row = this.row_widget as RowRepositoryGrid;
      if ( row != null ) {
        row.set_key ( key );
      }
    }

    public void set_keys ( ListBox keys ) {
      var row = this.row_widget as RowRepositoryGrid;
      if ( row != null ) {
        row.set_keys ( keys );
      }
    }

    public Section to_config_repository () {
      var row = this.row_widget as RowRepositoryGrid;
      var repo = new Section ( "" );
      if ( row != null ) {
        repo = new Section ( row.get_name () );

        repo.set_commented ( this.get_enabled () ? Commented.NO : Commented.YES  );

        for ( int i = 0; i < row.get_keys ().length; i++ ) {
          RowLabelLabel row_key = row.get_keys ().get_row_at_index ( i ).get_child () as RowLabelLabel;

          var key = new Key ( row_key.get_key (), row_key.get_value (), KeyWithValue.YES );

          repo.add_key ( key );
        }
      }

      return repo;
    }

    private void complete_edit_form () {
      var row = this.row_widget as RowRepositoryGrid;
      if ( row != null ) {
        this.edit_form.insert_row_label_entry ( _("Name" ), row.get_name () );

        for ( int i = 0; i < row.get_keys ().length; i++ ) {
          RowLabelLabel row_key = row.get_keys ().get_row_at_index ( i ).get_child () as RowLabelLabel;

          this.edit_form.insert_row_combobox_entry ( row_key.get_key (), row_key.get_value () );
        }
      }
    }

    private void save_edit () {
      var keys = this.edit_form.get_listbox ();

      this.set_keys ( keys );

      keys.clear ();

      this.changed ();
    }

    private void complete_delete_form () {
      var row = this.row_widget as RowRepositoryGrid;
      if ( row != null ) {
        this.label_delete_info.set_label ( row.get_name () );
      }
    }
  }
}
