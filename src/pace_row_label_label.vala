/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_label_label.vala
 *
 * Copyright 2018 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_row_label_label.ui" )]
  public class RowLabelLabel : RowLabel, RowKeyValue {
    [GtkChild]
    private Gtk.Label label_value;
    public Gtk.Align label_value_halign {
      get { return this.label_value.get_halign (); }
      set { this.label_value.set_halign ( value ); }
    }

    public RowLabelLabel ( string key, string value = "" ) {
      base ( key );

      this.label_value.set_label ( value );
    }

    public void set_key ( string key ) {
      this.set_label_key ( key );
    }

    public string get_key () {
      return this.get_label_key ();
    }

    public string get_value () {
      return this.label_value.get_label ();
    }

    public void set_value ( string value ) {
      this.label_value.set_label ( value );
    }
  }
}
