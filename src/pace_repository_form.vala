/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_repository_form.vala
 *
 * Copyright 2018-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_repository_form.ui" )]
  public class RepositoryForm : Gtk.Grid {
    [GtkChild]
    private ListBoxFixed listbox;
    [GtkChild]
    private Gtk.Button button_save;
    public bool toolbar_add_button_visible {
      get { return this.listbox.toolbar_add_button_visible; }
      set { this.listbox.toolbar_add_button_visible = value; }
    }

    public signal void save ();
    public signal void cancel ();

    public RepositoryForm () {
    }

    public void clear () {
      this.listbox.clear ();
    }

    public void insert_row_combobox_entry ( string key, string key_value = "" ) {
      this.listbox.insert_row_combobox_entry ( key, -1, key_value );
    }

    public void insert_row_label_entry ( string key, string key_value = "" ) {
      this.listbox.insert_row_label_entry ( key, -1, key_value );
    }

    public ListBoxFixed get_listbox () {
      return this.listbox;
    }

    public void set_buttons_for_add () {
      this.button_save.get_style_context ().add_class ( "suggested-action" );
    }

    public void set_buttons_for_edit () {
      this.listbox.toolbar_add_button_visible = true;
      this.button_save.get_style_context ().add_class ( "destructive-action" );
    }

    [GtkCallback]
    private void check_empty_name () {
      RowKeyValue row = this.listbox.get_row_at_index ( 0 ).get_child () as RowKeyValue;
      if ( row.get_key ().strip () == _("Name" ) ) {
        if ( row.get_value ().strip () != "" ) {
          this.button_save.set_sensitive ( true );
        } else {
          this.button_save.set_sensitive ( false );
        }
      }
    }

    [GtkCallback]
    private void on_button_save_clicked () {
      this.save ();
    }

    [GtkCallback]
    private void on_button_cancel_clicked () {
      this.clear ();

      this.cancel ();
    }
  }
}
