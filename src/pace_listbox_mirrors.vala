/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_listbox_mirrors.vala
 *
 * Copyright 2019 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using IniParser;

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_listbox_mirrors.ui" )]
  public class ListBoxMirrors : Gtk.Grid {
    [GtkChild]
    private Gtk.Label label;
    [GtkChild]
    private ListBoxSortable listbox;
    private ConfigFile mirrorlist_file;
    private Array<string> initial_order;

    public signal void mirrors_changed ();

    public ListBoxMirrors ( string path_to_mirrorlist_file ) {
      this.mirrorlist_file = new ConfigFile ( path_to_mirrorlist_file );
      this.initial_order = new Array<string> ();
      this.label.set_label ( path_to_mirrorlist_file );

      this.set_mirrors ();
    }

    private void set_mirrors () {
      this.listbox.clear ();

      var mirrors = this.mirrorlist_file.get_no_section_keys ();

      for ( int i=0; i < mirrors.length; i++ ) {
        var mirror = mirrors.index ( i );

        if ( mirror.get_key () == "Server" ) {
          var mirror_row = new_mirror ();
          mirror_row.set_mirror_data ( mirror );

          this.listbox.insert ( mirror_row, -1 );
          this.initial_order.append_val ( mirror.get_value () );
        }
      }
    }

    public ConfigFile get_mirrorlist_file () {
      int i = 0;
      RowMirror row_mirror;
      var mirrors = new Array<Key> ();

      var row = this.listbox.get_row_at_index ( i );

      while ( row != null ) {
        row_mirror = row.get_child () as RowMirror;

        var mirror = row_mirror.to_config_key ();

        mirrors.append_val ( mirror );

        i++;
        row = this.listbox.get_row_at_index ( i );
      }

      this.mirrorlist_file.set_no_section_keys ( mirrors );

      return this.mirrorlist_file;
    }

    private void row_to_remove ( RowSortable row ) {
      Gtk.ListBoxRow listbox_row = row.get_parent () as Gtk.ListBoxRow;

      this.listbox.remove_listboxrow ( listbox_row );
    }

    [GtkCallback]
    private void file_changed () {
      this.mirrors_changed ();
    }

    public bool get_changed () {
      bool changed = false;
      int i = 0;
      var row = this.listbox.get_row_at_index ( i );

      while ( row != null && i < this.initial_order.length ) {
        var row_mirror = row.get_child () as RowMirror;

        if ( row_mirror.get_changed () ||
             row_mirror.to_config_key ().get_value () != this.initial_order.index ( i ) ) {
          changed = true;
          break;
        }

        i++;
        row = this.listbox.get_row_at_index ( i );
      }

      return changed;
    }

    private RowMirror new_mirror () {
      var row = new RowMirror ();

      row.changed.connect ( this.file_changed );
      row.to_remove.connect ( this.row_to_remove );

      return row;
    }

    [GtkCallback]
    private void on_clicked_button_add_row () {
      var mirror = this.new_mirror ();

      mirror.new_empty ();

      this.listbox.insert ( mirror, -1 );
    }

    [GtkCallback]
    private void on_row_activated ( Gtk.ListBoxRow row ) {
      RowMirror mirror = row.get_child () as RowMirror;

      mirror.on_row_activated ();
    }
  }
}
