namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_configuration.ui" )]
  public class Configuration : Gtk.Grid {
    [GtkChild]
    private Gtk.Switch switch_enable_preview;
    [GtkChild]
    private Gtk.Switch switch_full_preview;
    private GLib.Settings settings;

    construct {
      this.settings = new Settings ( "ar.com.softwareperonista.Pace" );

      this.load_settings ();
    }

    public bool get_preview_enable_state () {
      return this.switch_enable_preview.get_active ();
    }

    private void load_settings () {
      this.get_enable_preview ();
      if ( this.diff_exists () ) {
        this.get_full_preview ();
      } else {
        this.settings.set_boolean ( "full-preview", false );
        this.switch_full_preview.parent.parent.set_visible ( false );
      }
    }

    private void get_enable_preview () {
      this.switch_enable_preview.set_active ( this.settings.get_boolean ( "enable-preview" ) );
    }

    private void get_full_preview () {
      this.switch_full_preview.set_active ( this.settings.get_boolean ( "full-preview" ) );
    }

    [GtkCallback]
    private void set_enable_preview () {
      this.settings.set_boolean ( "enable-preview", this.switch_enable_preview.get_active () );
    }

    [GtkCallback]
    private void set_full_preview () {
      this.settings.set_boolean ( "full-preview", this.switch_full_preview.get_active () );
    }

    private bool diff_exists () {
      var diff = File.new_for_path ( "/usr/bin/diff" );

      return diff.query_exists ();
    }
  }
}
