/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_row_sortable.vala
 *
 * Copyright 2020 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_row_sortable.ui" )]
  public class RowSortable : RowActivatable {
    [GtkChild]
    private Gtk.Grid grid_row;
    [GtkChild]
    private Gtk.EventBox eventbox_drag_n_drop;
    [GtkChild]
    private Gtk.Stack stack;
    [GtkChild]
    protected RepositoryForm edit_form;
    [GtkChild]
    protected Gtk.Label label_delete_message;
    [GtkChild]
    protected Gtk.Label label_delete_info;
    [GtkChild]
    private Gtk.Grid grid_delete;
    private string original_delete_message;

    protected Gtk.Widget row_widget {
      get { return this.grid_row.get_child_at ( 0, 0 ); }
      set { this.grid_row.attach ( value, 0, 0 ); }
    }

    public signal void button_edit_clicked ();
    public signal void button_delete_clicked ();
    public signal void edit_save ();
    public signal void edit_cancel ();
    public signal void drag_start ( Gtk.ListBoxRow row );
    public signal void drag_over ();
    public signal void to_remove ( RowSortable row );

    construct {
      this.original_delete_message = this.label_delete_message.get_label ();
      Gtk.TargetEntry targets[] = {
        { "GTK_LIST_BOX_ROW", Gtk.TargetFlags.SAME_APP, 0 }
      };

      Gtk.drag_source_set ( this.eventbox_drag_n_drop,
                            Gdk.ModifierType.BUTTON1_MASK,
                            targets, Gdk.DragAction.MOVE );
    }

    protected void set_delete_message ( string text ) {
      this.label_delete_message.set_label ( this.original_delete_message + " " + text + "?" );
    }

    [GtkCallback]
    private void on_drag_begin ( Gdk.DragContext context ) {
      int x, y;
      var listbox = this.get_ancestor ( new Gtk.ListBox ().get_type () );
      var row = this.get_ancestor ( new Gtk.ListBoxRow ().get_type () );

      var alloc = Gtk.Allocation ();
      row.get_allocation ( out alloc );

      var surface = new Cairo.ImageSurface ( Cairo.Format.ARGB32, alloc.width, alloc.height );
      var cr = new Cairo.Context ( surface );

      listbox.get_style_context ().render_background ( cr, 0, 0, alloc.width, alloc.height );
      row.get_style_context ().add_class ( "frame" );
      row.draw ( cr );
      row.get_style_context ().remove_class ( "frame" );
      row.set_visible ( false );

      this.eventbox_drag_n_drop.translate_coordinates ( row, 0, 0, out x, out y );
      surface.set_device_offset ( -x, -y );

      Gtk.drag_set_icon_surface ( context, surface );
      this.drag_start ( row as Gtk.ListBoxRow );
    }

    [GtkCallback]
    private void on_drag_end ( Gdk.DragContext context ) {
      var row = this.get_ancestor ( new Gtk.ListBoxRow ().get_type () );
      row.set_visible ( true );
      this.drag_over ();
    }

    [GtkCallback]
    private void on_drag_data_get ( Gdk.DragContext context, Gtk.SelectionData selection_data, uint info, uint time_ ) {
      var row = this.get_ancestor ( new Gtk.ListBoxRow ().get_type () );

      uchar[] data = new uchar[(sizeof (Gtk.Widget) )];
      ( (Gtk.Widget[])data)[0] = row;
      selection_data.set ( Gdk.Atom.intern_static_string ( "GTK_LIST_BOX_ROW" ), 32, data );
    }

    [GtkCallback]
    protected void on_button_edit_clicked () {
      this.button_edit_clicked ();
      this.edit_form.set_visible ( true );

      this.stack.set_visible_child_name ( "page_edit" );
    }

    [GtkCallback]
    private void on_edit_cancel () {
      this.edit_form.set_visible ( false );
      this.stack.set_visible_child_name ( "page_row" );
      this.edit_cancel ();
    }

    [GtkCallback]
    private void on_edit_save () {
      this.edit_save ();
      this.on_edit_cancel ();
    }

    [GtkCallback]
    private void on_button_delete_clicked () {
      this.button_delete_clicked ();
      this.grid_delete.set_visible ( true );
      this.stack.set_visible_child_name ( "page_delete" );
    }

    [GtkCallback]
    private void on_button_delete_cancel_clicked () {
      this.grid_delete.set_visible ( false );
      this.stack.set_visible_child_name ( "page_row" );
    }

    [GtkCallback]
    protected void on_button_delete_accept_clicked () {
      this.to_remove ( this );
    }
  }
}
