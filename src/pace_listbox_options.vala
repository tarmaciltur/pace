/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pace_listbox_options.vala
 *
 * Copyright 2018-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using IniParser;

namespace Pace {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/pace/ui/pace_listbox_options.ui" )]
  public class ListBoxOptions : Gtk.Grid, DataStatus {
    [GtkChild]
    private ListBoxFixed listbox;
    public string file_path;

    construct {
      this.file_path = "";
    }

    public Array<Key> get_options () {
      int i = 0;
      RowOption row_option;
      var options = new Array<Key> ();

      var row = this.listbox.get_row_at_index ( i );

      while ( row != null ) {
        row_option = row.get_child () as RowOption;

        var option = row_option.to_config_key ();

        options.append_val ( option );

        i++;
        row = this.listbox.get_row_at_index ( i );
      }

      return options;
    }

    public void set_options ( ConfigFile config_file ) {
      this.listbox.clear ();

      this.file_path = config_file.get_path ();

      var options = config_file.get_options ();

      for ( int i=0; i < options.length; i++ ) {
        var option = options.index ( i );
        var option_row = new RowOption ( option );

        option_row.changed.connect ( this.file_changed );

        this.listbox.insert ( option_row, -1 );
      }
    }

    public bool get_changed () {
      return this.listbox.get_changed ();
    }

    public string get_file_path () {
      return this.file_path;
    }

    private void file_changed () {
      this.change ();
    }

    [GtkCallback]
    private void on_row_activated ( Gtk.ListBoxRow row ) {
      var child = row.get_child () as RowActivatable;

      child.on_row_activated ();
    }
  }
}
