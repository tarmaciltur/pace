/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceConfigFileTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    PaceConfigFileTest.add_tests ();

    string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                          "[repo1]\nServer = value\n\n" +
                          "#[repo2]\n#Server = value\n#Include = value2";

    try {
      FileUtils.set_contents ( "config_file_test.conf", file_content );
    } catch ( Error e ) {
      stderr.printf ( "Error creating config file (%s)\n", e.message );
    }

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/config_file", () => {
      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );

      assert ( test_file is ConfigFile );
    } );

    Test.add_func ( "/pace/config_file/string", () => {
      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );

      assert ( test_file.to_string () == expected_string );
    } );

    Test.add_func ( "/pace/config_file/repo_keys", () => {
      string expected_string = "Repo: repo1\n\tServer = value\n" +
                               "Repo: repo2\n\tServer = value\n\tInclude = value2\n";

      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );
      var repos = test_file.get_repositories ();
      string output = "";
      for ( int i = 0; i < repos.length; i++ ) {
        var repo = repos.index ( i );
        output += "Repo: " + repo.get_name () + "\n";
        var keys = repo.get_keys ();
        for ( int j = 0; j < keys.length; j++ ) {
          var key = keys.index ( j );
          output += "\t" + key.get_key () + " = " + key.get_value () + "\n";
        }
      }
      assert ( output == expected_string );
    } );

    Test.add_func ( "/pace/config_file/enable_repo", () => {
      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "[repo2]\nServer = value\nInclude = value2\n";

      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );
      test_file.enable_repository ( "repo2", true );

      assert ( test_file.to_string () == expected_string );
    } );

    Test.add_func ( "/pace/config_file/disable_repo", () => {
      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "# [repo1]\n# Server = value\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );
      test_file.enable_repository ( "repo1", false );

      assert ( test_file.to_string () == expected_string );
    } );

    Test.add_func ( "/pace/config_file/remove_repository", () => {
      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );
      test_file.remove_repository ( "repo1" );
      assert ( test_file.to_string () == expected_string );
    } );

    Test.add_func ( "/pace/config_file/changed/no_change", () => {
      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );

      assert ( test_file.changed () == false );
    } );

    Test.add_func ( "/pace/config_file/changed/change/repository", () => {
      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );
      test_file.enable_repository ( "repo1", false );

      assert ( test_file.changed () == true );
    } );

    Test.add_func ( "/pace/config_file/changed/change/unexistent_repository", () => {
      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );
      test_file.enable_repository ( "unexistent_repository", false );

      assert ( test_file.changed () == false );
    } );

    Test.add_func ( "/pace/config_file/changed/change/option", () => {
      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );
      test_file.unset_option ( "key2" );

      assert ( test_file.changed () == true );
    } );

    Test.add_func ( "/pace/config_file/changed/reverted_change/repository", () => {
      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );
      test_file.enable_repository ( "repo1", false );
      test_file.enable_repository ( "repo1", true );

      assert ( test_file.changed () == false );
    } );

    Test.add_func ( "/pace/config_file/changed/reverted_change/option", () => {
      ConfigFile test_file = new ConfigFile ( "config_file_test.conf" );
      test_file.unset_option ( "key2" );
      test_file.set_option ( "key2", "value2" );

      assert ( test_file.changed () == false );
    } );
  }
}
