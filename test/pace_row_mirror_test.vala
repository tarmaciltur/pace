/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;
using IniParser;

class PaceRowMirrorTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceRowMirrorTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/listboxrow_mirror", () => {

      RowMirror test_row = new RowMirror ();

      assert ( test_row is RowMirror );
    } );

    Test.add_func ( "/pace/listboxrow_mirror/to_config_key", () => {
      Key test_key = new Key ( "Server", "value", KeyWithValue.YES, Commented.NO );

      RowMirror test_row = new RowMirror ();

      test_row.set_mirror_data ( test_key );

      Key row_returned_key = test_row.to_config_key ();

      assert ( row_returned_key.to_string () == test_key.to_string () );
    } );
  }
}
