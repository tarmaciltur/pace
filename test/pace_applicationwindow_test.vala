/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceApplicationWindowTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceApplicationWindowTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/applicationwindow", () => {
      var app = new Gtk.Application ( "ar.com.softwareperonista.Pace", ApplicationFlags.FLAGS_NONE );
      app.activate.connect ( () => {
        var win = app.active_window;
        if ( win == null ) {
          ApplicationWindow test_applicationwindow = new ApplicationWindow ( app );
          assert ( test_applicationwindow is ApplicationWindow );
        }
      } );
    } );
  }
}
