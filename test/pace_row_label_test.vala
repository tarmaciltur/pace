/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceRowLabelTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceRowLabelTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/row_label", () => {
      RowLabel test_row_label = new RowLabel ( "key" );

      assert ( test_row_label is RowLabel );
    } );

    Test.add_func ( "/pace/row_label/get_label_key", () => {
      RowLabel test_row_label = new RowLabel ( "key" );

      assert ( test_row_label.get_label_key () == "key" );
    } );

    Test.add_func ( "/pace/row_label/set_key", () => {
      RowLabel test_row_label = new RowLabel ( "key" );

      test_row_label.set_label_key ( "key2" );

      assert ( test_row_label.get_label_key () == "key2" );
    } );
  }
}
