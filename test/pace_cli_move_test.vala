/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceCliMoveTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    PaceCliMoveTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/cli/move/up", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_up.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo2", "--up"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/up/first", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_up_first.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo1", "--up"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/up/missing_repo", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_up_missing_repo.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo4", "--up"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/down", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_down.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo2", "--down"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/down/last", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_down_last.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo3", "--down"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/down/missing_repo", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_down_missing_repo.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo4", "--up"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/first", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_first.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo2", "--first"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/first/first", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_first_first.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo1", "--first"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/first/missing_repo", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_first_missing_repo.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo4", "--first"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/last", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_last.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo2", "--last"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";


      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/last/last", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_last_last.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo3", "--last"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/last/missing_repo", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_last_missing_repo.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo4", "--last"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/above", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n" +
                            "#[repo3]\n#Server = value\n#Include = value2\n" +
                            "#[repo4]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_above.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo4", "--above", "repo2"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo4]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/above/2", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n" +
                            "#[repo3]\n#Server = value\n#Include = value2\n" +
                            "#[repo4]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_above.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo2", "--above", "repo4"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo4]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/above/self", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_above_above.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo2", "--above", "repo2"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/above/first", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_above_above.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo3", "--above", "repo1"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/above/missing_repo", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_above_missing_repo.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo4", "--above", "repo2"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/above/missing_repo/destination_repo", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_above_missing_repo.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo2", "--above", "repo4"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/below", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n" +
                            "#[repo3]\n#Server = value\n#Include = value2\n" +
                            "#[repo4]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_below.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo1", "--below", "repo3"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo4]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/below/2", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n" +
                            "#[repo3]\n#Server = value\n#Include = value2\n" +
                            "#[repo4]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_below.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo3", "--below", "repo1"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo4]\n# Server = value\n# Include = value2\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/below/self", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_below_self.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo2", "--below", "repo2"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/below/last", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_below_last.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo1", "--below", "repo3"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n# key = value\nkey2 = value2\noption\n\n" +
                               "# [repo2]\n# Server = value\n# Include = value2\n\n" +
                               "# [repo3]\n# Server = value\n# Include = value2\n\n" +
                               "[repo1]\nServer = value\n";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/below/missing_repo", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_below_missing_repo.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo4", "--below", "repo2"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );

    Test.add_func ( "/pace/cli/move/below/missing_repo/destination_repo", () => {
      string file_content = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                            "[repo1]\nServer = value\n\n" +
                            "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                            "#[repo3]\n#Server = value\n#Include = value2";
      string file_name = "cli_test_pacman_move_below_missing_repo.conf";
      PaceCliMoveTest.create_file ( file_name, file_content );
      string[] args = {"", "move", "repo2", "--below", "repo4"};


      Cli test_cli_list = new Cli ( file_name );

      test_cli_list.run ( args );

      string file_changed = read_file ( file_name );

      string expected_string = "# comment\n\n[options]\n#key = value\nkey2 = value2\noption\n\n" +
                               "[repo1]\nServer = value\n\n" +
                               "#[repo2]\n#Server = value\n#Include = value2\n\n" +
                               "#[repo3]\n#Server = value\n#Include = value2";

      assert ( file_changed == expected_string );
    } );
  }

  private static void create_file ( string name, string content ) {
    try {
      FileUtils.set_contents ( name, content );
    } catch ( Error e ) {
      stderr.printf ( "Error creating config file (%s)\n", e.message );
    }
  }

  private static string read_file ( string file_name ) {
    string file_content = "";
    try {
      FileUtils.get_contents ( file_name, out file_content );
    } catch ( Error e ) {
      stderr.printf ( "Error opening config file (%s)\n", e.message );
    }

    return file_content;
  }
}
