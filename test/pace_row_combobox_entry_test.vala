/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Pace;

class PaceRowComboBoxEntryTest {
  public static int main ( string[] args ) {
    Test.init ( ref args );

    if ( !Gtk.init_check ( ref args ) )
    {
      return 77; //meson exit code to skip test
    }

    PaceRowComboBoxEntryTest.add_tests ();

    Test.run ();

    return 0;
  }

  public static void add_tests () {
    Test.add_func ( "/pace/row_combobox_entry", () => {
      RowComboBoxEntry test_row_combobox_entry = new RowComboBoxEntry ( "Server" );

      assert ( test_row_combobox_entry is RowComboBoxEntry );
    } );

    Test.add_func ( "/pace/row_combobox_entry/get_key", () => {
      RowComboBoxEntry test_row_combobox_entry = new RowComboBoxEntry ( "Include" );

      assert ( test_row_combobox_entry.get_key () == "Include" );
    } );

    Test.add_func ( "/pace/row_combobox_entry/get_key/wrong_key_name", () => {
      RowComboBoxEntry test_row_combobox_entry = new RowComboBoxEntry ( "key" );

      assert ( test_row_combobox_entry.get_key () == "Server" );
    } );

    Test.add_func ( "/pace/row_combobox_entry/set_key", () => {
      RowComboBoxEntry test_row_combobox_entry = new RowComboBoxEntry ( "key" );

      test_row_combobox_entry.set_key ( "Server" );

      assert ( test_row_combobox_entry.get_key () == "Server" );
    } );

    Test.add_func ( "/pace/row_combobox_entry/set_key/get_key/wrong_key_name", () => {
      RowComboBoxEntry test_row_combobox_entry = new RowComboBoxEntry ( "Include" );

      test_row_combobox_entry.set_key ( "key2" );

      assert ( test_row_combobox_entry.get_key () == "Include" );
    } );

    Test.add_func ( "/pace/row_combobox_entry/get_value/unassigned", () => {
      RowComboBoxEntry test_row = new RowComboBoxEntry ( "Server" );

      assert ( test_row.get_value () == "" );
    } );

    Test.add_func ( "/pace/row_combobox_entry/get_value/constructor", () => {
      RowComboBoxEntry test_row = new RowComboBoxEntry ( "Server", "value" );

      assert ( test_row.get_value () == "value" );
    } );

    Test.add_func ( "/pace/row_combobox_entry/set_value", () => {
      RowComboBoxEntry test_row = new RowComboBoxEntry ( "Server" );

      test_row.set_value ( "value" );

      assert ( test_row.get_value () == "value" );
    } );
  }
}
